package backendAutomation;

import java.util.*;

public class SortedSentence {
	static String sortSentence(String sentence) {
		StringBuilder sorted = new StringBuilder();
		String[] words = sentence.split(" ");
		HashMap<Integer, StringBuilder> wordMap = new HashMap<>();

		for (String word : words) {
			int charSum = getCharSum(word);
			if (wordMap.containsKey(charSum)) {
				wordMap.put(charSum, wordMap.get(charSum).append(" ").append(word));
			} else {
				wordMap.put(charSum, new StringBuilder(word));
			}
		}

		Object[] sortedKeys = wordMap.keySet().toArray();
		Arrays.sort(sortedKeys);

		for (Object i : sortedKeys) {
			sorted.append(" ").append(wordMap.get(i));
		}


		return sorted.toString();
	}

	private static int getCharSum(String word) {
		int sum = 0;
		for (int i = 0; i < word.length(); ++i) {
			sum += word.charAt(i) - 96; //assuming all lower case
		}
		return sum;
	}

	public static void main(String[] args) {
		System.out.println(sortSentence("a cat runs faster than the tortoise"));
		System.out.println(sortSentence("the enemy is rooting for a dot ball"));
		List<String> shuffled = Arrays.asList("aaaaaaaa bbbbbbbb cccccccc dddddddd eeeeeeee ffffffff gggggggg hhhhhhhh iiiiiiii jjjjjjjj kkkkkkkk llllllll mmmmmmmm nnnnnnnn oooooooo pppppppp qqqqqqqq rrrrrrrr ssssssss tttttttt uuuuuuuu vvvvvvvv wwwwwwww xxxxxxxx yyyyyyyy zzzzzzzz".split(" "));
		Collections.shuffle(shuffled);
		System.out.println(sortSentence(String.join(" ", shuffled)));
	}

}
