package task2CheckInput;

public class CheckInput {
	private static void checkInput(String type, int amount) {
		try {
			Wage WageType = Wage.valueOf(type);
			System.out.println(WageType.label + " is " + WageType.isValid(amount));
		} catch (IllegalArgumentException e) {
			System.out.println("Invalid Type: " + type);
		}
	}

	public static void main(String[] args) {
		checkInput("HOUR", 30);
		checkInput("HOUR", 9);
		checkInput("HOUR", 10);
		checkInput("HOUR", 100);
		checkInput("HOUR", 101);
		checkInput("HOURS", 101);
	}
}
