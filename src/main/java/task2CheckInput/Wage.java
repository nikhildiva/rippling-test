package task2CheckInput;

public enum Wage {

	HOUR("Hourly Wage", 10, 100),
	DAY("Daily Wage", 70, 600),
	WEEK("Weekly Wage", 300, 3000),
	MONTH("Monthly Salary", 1200, 12000),
	YEAR("Annual Salary", 14000, 140000);

	int lowerLimit;
	int upperLimit;
	String label;

	Wage(String label, int lowerLimit, int upperLimit) {
		this.lowerLimit = lowerLimit;
		this.upperLimit = upperLimit;
		this.label = label;
	}

	String isValid(int amount) {
		return (amount >= lowerLimit && amount <= upperLimit) ? "valid" : "invalid";
	}
}
