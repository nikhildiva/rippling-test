package task1Selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class FieldsPage {

	public FieldsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.CSS, using = "#numbers")
	public WebElement numberField;

	@FindBy(how = How.CSS, using = "#phone")
	public WebElement phoneField;

	public String getOutput(WebElement field, String input) {
		field.clear();
		field.sendKeys(input);
		return field.getAttribute("value");
	}

}
