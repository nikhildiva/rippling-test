package task1Selenium.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import task1Selenium.pages.FieldsPage;

public class FieldValidation {

	private WebDriver driver;
	private FieldsPage fieldsPage;

	@BeforeClass
	void setup() {
		//gecko driver might need replacement as per firefox version, os, architecture
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/src/test/resources/geckodriver");
		driver = new FirefoxDriver();
		String url = "https://vanilla-masker.github.io/vanilla-masker/demo.html";
		driver.get(url);
		fieldsPage = new FieldsPage(driver);
	}

	@DataProvider
	Object[][] phoneFieldData() {
		return new Object[][]{
				// happy flow
				{"0123456789", "(01) 2345-6789"},

				//bva+equivalence
				{"1", "(1"},
				{"12", "(12"},
				{"123", "(12) 3"},
				{"1234", "(12) 34"},
				{"12345", "(12) 345"},
				{"123456", "(12) 3456"},
				{"1234567", "(12) 3456-7"},
				{"12345678", "(12) 3456-78"},
				{"123456789", "(12) 3456-789"},
				{"123456789", "(12) 3456-789"},
				{"1234567890", "(12) 3456-7890"},
				{"1234567890123", "(12) 3456-7890"},

				//negative
				{"((", ""},
				{"))", ""},
				{"--", ""},
				{"((1))", "(1"},
				{"12a", "(12"},
				{"12*", "(12"},
				{"12.", "(12"},
				{"12-", "(12"},
				{"12)", "(12"},
				{"12)123123aa23", "(12) 1231-23"},
				{"12)123123**23", "(12) 1231-2323"},
				{"12)123123--23", "(12) 1231-2323"},
				{"12)123123aa23**", "(12) 1231-23"},
				{"12)123123**23--", "(12) 1231-2323"},
				{"12)123123--23))", "(12) 1231-2323"},
		};
	}


	@DataProvider
	Object[][] numbersOnlyFieldData() {

		return new Object[][]{
				//Happy path
				{"01234567890012345678900123456789001234567890", "01234567890012345678900123456789001234567890"}, //string of reasonable length

				//negative

				{"01234.567890012345678900123456789001234567890", "01234567890012345678900123456789001234567890"},  //decimal point
				{"abc01234/*-+.567890012asdd34567890012~!@#$%^&*()_+=-\\tqwertyuiop3456asdfghjkl7890zxcvbnm,./<>?012;':\"[]34{}567890", "01234567890012345678900123456789001234567890"},  //starting with alpha + special characters, escape seq in between
				{"*&%abc01234/*-+.567890012asdd34567890012~!@#$%^&*()_+=-\\tqwertyuiop3456asdfghjkl7890zxcvbnm,./<>?012;':\"[]34{}567890$#!abc", "01234567890012345678900123456789001234567890"},  //starting with special cahar, ending with alpha
				{"*&%abc01234/*-+.567890012asdd34567890012~!@#$%^&*()_+=-\\tqwertyuiop3456asdfghjkl7890zxcvbnm,./<>?012;':\"[]34{}567890aca:&$", "01234567890012345678900123456789001234567890"},  //ending with special
		};
	}


	@Test(dataProvider = "phoneFieldData")
	void phoneFieldValidation(String input, String expectedValue) {
		String actualValue = fieldsPage.getOutput(fieldsPage.phoneField, input);
		assert actualValue.equals(expectedValue) : "expected Value: " + expectedValue + "\nActual Value: " + actualValue;
	}


	@Test(dataProvider = "numbersOnlyFieldData")
	void numbersOnlyFieldValidation(String input, String expectedValue) {
		String actualValue = fieldsPage.getOutput(fieldsPage.numberField, input);
		assert actualValue.equals(expectedValue) : "expected Value: " + expectedValue + "\nActual Value: " + actualValue;
	}

	@AfterClass
	void tearDown() {
		driver.quit();
	}

}
